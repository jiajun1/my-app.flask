from flask import Flask, session
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import Response
from flask import jsonify
from flask import request
from flask import make_response
from flask import redirect
from flask import json
from flask import render_template
from flask import url_for
import redis

from flask_session import Session

from flask_cors import CORS

api_version = "/api/v1.0"

app = Flask(__name__)

CORS(app)

# celery = Celery(
#     __name__,
#     broker="redis://redis:6379/0",
#     backend="redis://redis:6379/0"
# )

app.secret_key = 'MY_APP_SECRET_KEY'
app.config["SESSION_TYPE"] = "redis"
app.config['SESSION_PERMANENT'] = False
app.config['SESSION_USE_SIGNER'] = True
app.config['SESSION_REDIS'] = redis.from_url('redis://redis:6379')


# sample users list
users_database = [
    {
        'username': 'default',
        'password': 'default',
        'firstname': 'default',
        'lastname': 'default',
    },
    {
        'username': '123',
        'password': '123',
        'firstname': 'Kevin',
        'lastname': 'Summers',
    }
]

#home page
@app.route('/')
def home():
    return render_template('index.html')

#returns a list of users in the database
@app.route(api_version + '/users')
def get_users():
  return jsonify({'users': users_database})

#validate the login request
@app.route(api_version + '/login', methods=['POST'])
def validate_login():
    request_data = request.get_json()
    for user in users_database:
        #if there is a same username in the databse, return true
        if user['username'] == request_data['username'] and user['password'] == request_data['password']:
            session['username'] = request_data['username']
            return jsonify({'message': True})
    #else return false
    return jsonify({'message': False})

#manage user logout and remove session
@app.route('/logout')
def logout():
    session.pop('username', default=None)
    return jsonify({'message': True}) 

#get username to display on profile page
@app.route('/profile/get_username')
def getUsername():
    username = {"username": session.get('username', default=None)}
    return Response(json.dumps(username), status=200)

#manage the register request
@app.route(api_version + '/register', methods=['POST'])
def register():
    request_data = request.get_json()
    for user in users_database:
        #if username already exist, return false
        if user['username'] == request_data['username']:
            return jsonify({'message': False})
    
    #if username do not exist, create new user, return true
    new_user = {
        'username': request_data['username'],
        'password': request_data['password'],
        'firstname': request_data['firstname'],
        'lastname': request_data['lastname']
    }
    users_database.append(new_user)
    return jsonify({'message': True})

#manage the update password request
@app.route('/update/<string:username>', methods=['POST'])
def forgot(username):
    request_data = request.get_json()
    for user in users_database:
        #if username exists, update the password
        if user['username'] == request_data['username']:
            new_password = {'password': request_data['new_password']}
            user.update(new_password)
            return jsonify({'message': True})
        else:
            return jsonify({'message': False})
    return jsonify({'message': False})


@app.route(api_version + '/reset_password/<string:username>', methods=['POST'])
def check_username(username):
    request_data = request.get_json()
    for user in users_database:
        #check if username exists
        if user['username'] == request_data['username']:
            return jsonify({'message': True})
        else:
            return jsonify({'message': False})

# Generate token with given username
@app.route(api_version + '/generate_token/<string:username>')
def generate_token(username, expires_sec=3600):
    serial = Serializer(app.config['SECRET_KEY'], expires_in = expires_sec)
    return serial.dumps({'confirm': username}).decode('utf-8')

#Confirm the token
@app.route(api_version + '/confirm_token', methods=['GET'])
def confirm_token(self, token):
    serial = Serializer(app.config['SECRET_KEY'])
    try:
        data = serial.load(token)
    except:
        return False
    if data.get('confirm') != self.username:
        return False
    

if __name__ == '__main__':
    app.run(port=5000, debug=True)